# Movesense iOS and Android Libraries  

## News  
**(Apr 15th 2020) Update 1.44.0 to mobile libraries released **
 
Android Showcase app (ex _sampleapp_) has been updated to version 1.9.9. Please find the apk in the "Downloads" section.
Please refer to [CHANGES.md](CHANGES.md) for more details.

iOS ShowcaseApp source code is published under Apache 2.0 license.  
Corresponding app always available in [App Store](https://apps.apple.com/us/app/movesense-showcase/id1439876677)

**New documentation**  
[New Movesense documentation](http://movesense.com/docs) is now available. Any feedback is wellcome!

**List of Movesense compatible mobile devices available**  
[Movesense documentation](http://movesense.com/docs/system/compatibility_results/) contains a list of compatible mobile devices.

**Official Unity3D plugin**  
Check out news in [Movesense website](https://www.movesense.com/news/2018/09/movesense-plugin-for-unity3d/)

## 3rd party plugins

**React Native support**  
Check out Tugberk Akdogan's implementation on [Github](https://github.com/tugberka/react-native-mds)

**Xamarin plugin**  
Check out Andy Wigleys implementation in [Githib](https://github.com/AndyCW/MovesenseDotNet)


## Platform specific readme files can be found under '/android' and '/IOS' directories.